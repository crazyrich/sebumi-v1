<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function contactPage()
    {
        return view('users.menu.contact');
    }
}
