<div id="topmarkotop">
    <nav class="navbar navbar-expand-lg">
        <div class="container  set-pd">
            <a class="navbar-brand white" href="#" style="margin-left:15px;">
                <!-- <img src="assets/image/bahan/logo-s.png" height="30px" width="30px" class="rounded" style="margin-top: -5px;"> -->
                <img src="assets/image/logo_putih1.png"  class="rounded" style="margin-top: -10px; width: 110px;">
                <!-- <span class="font-24 bold">sebumi</span>  -->
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon putih"><i class="fa fa-bars" aria-hidden="true"></i></span>
            </button>

            <div class="collapse navbar-collapse flex-right " id="navbarSupportedContent" style="padding: 0px 10px;">
                <ul class="navbar-nav font-16">                        
                    <li class="nav-item">
                        <a class="nav-link white bold" href="#about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link white bold " href="{{ url('/package') }}">Packages</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link white bold " href="#service">Our Stories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link white bold" href="#portfolio">Sebumi Berbagi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link white bold" href="{{ url('/contact') }}">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link white bold" href="#testimony">Log In</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>