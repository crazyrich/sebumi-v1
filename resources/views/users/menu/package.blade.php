@extends('users.master')
@section('konten')
<header class="set-header">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators bottom1">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="assets/image/bahan/forest.jpg" alt="Second slide">
                <div class="carousel-caption d-none d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="assets/image/bahan/gunung.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="assets/image/bahan/5.jpg" alt="Second slide">
                <div class="carousel-caption d-none d-md-block kiri text-nav">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="bold font-70 l-h-75 m-b-15">
                                Our <br>
                                Packages
                            </div>
                            <div>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</header>

<section id="service" class=" section-padding ">
    <div class="container  set-pd">
        <div class="tengah m-b-50">
            <div class="font-36 bold abu1">Travel with Us</div>
            <div class="bold abu1">OUR PACKAGES CATEGORIES</div>
        </div>

        <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel" style="    height: 600px;">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active" style="height:600px">
                        <div class="carousel-caption d-none d-md-block kiri text-nav2" style="height:600px; left: 0%;">
                            <div class="row m-b-50 set-margin">
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/8.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Custom Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/9.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>School Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">3 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with4.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/9.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Corporate Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">3 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with5.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" style="height:600px">
                        <div class="carousel-caption d-none d-md-block kiri text-nav2" style="height:600px; left: 0%;">
                            <div class="row m-b-50 set-margin">
                                <div class="col-sm-4 m-b-20">
                                        <div class="card n-card tes1">
                                                
                                            <div class="overlay">
                                                <div class="m-b-20 pd-20 mt-10s">
                                                    <div class="font-16 text-center">
                                                        <img src="assets/image/icon/6.png" width="70px">
                                                    </div>
                                                    <div class="font-14 text-center m-b-10">
                                                        <div class="font-26" style="line-height: 33px;"><b>Sustainability Workshop</b></div>
                                                    </div>
                                                    <div class="font-12 text-center">2 TOURS</div>
                                                </div>
                                            </div>
            
                                            <img src="assets/image/bahan/with1.PNG" alt="..." class="img-thumbnail wt-us">
                                        </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/7.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Signature Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">3 TOURS</div>
                                            </div>
                                        </div>
        
                                        <img src="assets/image/bahan/with2.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                                <div class="col-sm-4 m-b-20">
                                    <div class="card n-card tes1">
                                        <div class="overlay">
                                            <div class="m-b-20 pd-20 mt-10s">
                                                <div class="font-16 text-center">
                                                    <img src="assets/image/icon/8.png" width="70px">
                                                </div>
                                                <div class="font-14 text-center m-b-10">
                                                    <div class="font-26" style="line-height: 33px;"><b>Custom Trip</b></div>
                                                </div>
                                                <div class="font-12 text-center">2 TOURS</div>
                                            </div>
                                        </div>
                                        <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators1" role="button" data-slide="prev" style="left: -3rem;">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators1" role="button" data-slide="next" style="right: -3rem;">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        <!-- <div class="row m-b-50 set-margin">
            <div class="col-sm-4 m-b-20">
                <div class="card n-card">
                    <img src="assets/image/bahan/with3.PNG" alt="..." class="img-thumbnail wt-us">
                </div>
            </div>
            <div class="col-sm-4 m-b-20">
                <div class="card n-card">
                    <img src="assets/image/bahan/with4.PNG" alt="..." class="img-thumbnail wt-us">
                </div>
            </div>
            <div class="col-sm-4 m-b-20">
                <div class="card n-card">
                    <img src="assets/image/bahan/with5.PNG" alt="..." class="img-thumbnail wt-us">
                </div>
            </div>
        </div> -->
    </div>
</section>
@endsection