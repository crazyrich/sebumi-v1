<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="{{ asset('assets/image/logo3.png') }}" width="25px" height="25px">
        <!-- Bootstrap CSS -->
        <!-- <link rel="stylesheet" href="assets/bootstrap4/dist/css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/sebumi.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
        <script type="text/javascript">
            $(window).on('scroll', function() {
                if($(window).scrollTop()){
                    $('nav').addClass('hitam');
                }else{
                    $('nav').removeClass('hitam');
                }
            })
            
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {scrollFunction()};

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("myBtn").style.display = "block";
                } else {
                    document.getElementById("myBtn").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }

            $(window).on("load",function() {
                $(window).scroll(function() {
                    var windowBottom = $(this).scrollTop() + $(this).innerHeight();
                    $(".fade").each(function() {
                    /* Check the location of each desired element */
                    var objectBottom = $(this).offset().top + $(this).outerHeight();
                    
                    /* If the element is completely within bounds of the window, fade it in */
                    if (objectBottom < windowBottom) { //object comes into view (scrolling down)
                        if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
                    } else { //object goes out of view (scrolling up)
                        if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
                    }
                    });
                }).scroll(); //invoke scroll-handler on page-load
            });

        </script>
        <style>
            html {
                scroll-behavior: smooth;
                transition: 2s;
            }
            .hitam {
                background:rgba(0, 0, 0, 0.7)!important;
            }

            #map {
            height: 100vh;
            }
            
            .gm-style .gm-style-iw-c {
                border-radius: 5px !important;
                padding: 0px !important;
            }
            .gm-style-iw-d {
                overflow: auto !important; 
                overflow-x: hidden !important; 
            }

        </style>

        <title>SEBUMI</title>
        @yield('ex-css')
    </head>
    <body>
        <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa  fa-lg fa-arrow-up" aria-hidden="true"></i></button>
        @include('users.header')
        @yield('konten')
        @include('users.footer')
        @yield('ex-js')
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBz7apE2tGz72HyUG7I-hgKCgl_3ZVxEPs&callback=initMap">
        </script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="assets/bootstrap4/dist/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="assets/js/jquery.bubble.text.js"></script>
        <script src="assets/js/script.js"></script>
    </body>
</html>