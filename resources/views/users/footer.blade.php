<footer class="footer">
    <div class="container set-pd">
        <div class="row m-b-30 set-margin">
            <div class="col-sm-3">
                <div class="m-b-5">
                    <!-- <img src="assets/image/bahan/logo-s.png" height="25px" width="25px" class="rounded" style="margin-top: -5px;">
                    <span class="font-24 bold putih">sebumi</span>  -->
                    <img src="assets/image/logo_putih1.png"  class="rounded" style="margin-top: -5px; width: 110px;">
                </div>
                <div class="putih font-14 m-b-30">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                </div>
                <div>
                    <span class="putih set-tw"><i class="fa fa-youtube-play fa-lg" aria-hidden="true"></i></span> 
                    <span class="putih set-fb"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></span> 
                    <span class="putih set-ig"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></span> 
                </div>
            </div>
            <div class="col-sm-3">
                <div class="m-b-15">
                    <span class="font-14 bold font-core">Contact Info</span> 
                </div>
                <div class="putih font-14 m-b-30">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                </div>
            </div>
            <div class="col-sm-3">
                <div class="m-b-15">
                    <span class="font-14 bold font-core">Career</span> 
                </div>
                <div class="putih font-14 m-b-30">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porta ante eget metus feugiat luctus.
                </div>
            </div>
            <div class="col-sm-3">
                <div class="m-b-15">
                    <span class="font-14 bold font-core">Newsletter</span> 
                </div>
                <div class="putih font-14 m-b-30">
                    <input type="text" class="m-b-10 set-form" placeholder="Your Email Address">
                    <button class="btn-search">Subcribe</button>
                </div>
            </div>
        </div>

        <div class="row set-margin" >
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 m-tengah">
                    <span class="putih font-12">&copy; Copyright Sebumi Berbagi</span>
            </div>
            <div id="footer-bawah" class="col-xl-9 col-lg-9 col-md-8 col-sm-12">
                <div class="kanan font-12 putih">
                    <ul id="menu-bawah">
                        <li class="m-l-20 cursor">Home</li>
                        <li class="m-l-20 cursor">About Us</li>
                        <li class="m-l-20 cursor">Packages</li>
                        <li class="m-l-20 cursor">Blogs</li>
                        <li class="m-l-20 cursor">Sebumi Berbagi</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>